FROM centos

## Git Instllation
RUN yum install git wget curl unzip -y

## AWS CLI v2 Installation
RUN curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip" && unzip awscliv2.zip && ./aws/install -i /usr/local/aws-cli -b /usr/local/bin
RUN aws --version

## Terraform 13 Installation
RUN wget https://releases.hashicorp.com/terraform/0.13.0/terraform_0.13.0_linux_amd64.zip && unzip terraform_0.13.0_linux_amd64.zip -d /usr/local/bin/
RUN terraform -v

## Helm 3.5.3 Installation
RUN wget https://get.helm.sh/helm-v3.5.3-linux-amd64.tar.gz && tar -xvzf helm-v3.5.3-linux-amd64.tar.gz && mv linux-amd64/helm /usr/local/bin/helm
RUN helm version

## AWS IAM Authenticatior Installation
RUN curl -o aws-iam-authenticator https://amazon-eks.s3.us-west-2.amazonaws.com/1.15.10/2020-02-22/bin/linux/amd64/aws-iam-authenticator && chmod +x ./aws-iam-authenticator && mv ./aws-iam-authenticator /usr/local/bin 
RUN aws-iam-authenticator help

## Kubectl 
RUN curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl" && install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl && mkdir -p ~/.local/bin/kubectl && mv ./kubectl ~/.local/bin/kubectl
RUN kubectl version --client
